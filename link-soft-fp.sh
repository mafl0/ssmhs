#!/usr/bin/env bash

#   'links' the ssm software float library by concatinating all *.ssm files in
# the PREFIX directory together. We will remove all comments and unnecessary
# whitespace
#
# Invoke like this:
#
#  $ link-soft-fp.sh program.ssm
#
# output is that of the linked program. The same output is also saved in
# program-sfp.ssm (assuming the example invokation).


FILE=${1} # File to link soft-fp with
FBASE=$(basename ${FILE})
FNAME="${FBASE%.*}"
PREFIX=${2:-testprograms/soft-fp}
OUTFILE=${FNAME}-sfp.ssm

if [[ ${FILE} == "" ]]; then
  echo "You have to supply 1 file to link with the software floating point library"
  exit 1
fi


echo "Making temporary copies"
mkdir -p /tmp/${PREFIX}
cp -r ${PREFIX}/* /tmp/${PREFIX}/
cp ${FILE} /tmp/${PREFIX}/${FBASE}
[ -e ${OUTFILE} ] && rm ${OUTFILE}

echo "Inserting \'bra main\'"
echo "bra main" >> ${OUTFILE}
echo "Linking..."
cat /tmp/${PREFIX}/*     |
  sed -E "/[ \t]*;.*/d"  |  # Remove comments, with optional leading white space
  sed "s/^[ \t]*//g"     |  # Remove leading white space
  sed '/^$/d' >> ${OUTFILE} # Remove empty lines

echo "Removing temporary files"
rm -r /tmp/${PREFIX}

echo "done."
exit 0
