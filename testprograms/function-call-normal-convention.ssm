; normal calling convention overview
bra main

; Normal calling convention stack layout
;
;           -----------------
; 0x00      argument 1          you have to push these arguments yourself
; 0x01       .
; 0x02       .
; 0x03      argument N
;           -----------------
; 0x04      return addr         bsr / jsr pushes this address (MP - 1 after link)
;           -----------------
; 0x05  MP  old mark pointer    link N creates this frame layout. unlink needs
; 0x06      local var 1          old MP for restoring the previous frame
; 0x07       .
; 0x08       .
; 0x09       .
; 0x0a      local var N
;           -----------------
; 0x0b  SP  subroutine stack   'local' stack of your subroutine
; 0x0c
;
; As you work through this example you will see that the normal calling
; convention has a lot of overhead. This is not necessarily fast and also the
; reason that inlining can mean such a huge speedup to your program.
;
; steps involved:
;
; 1. Load arguments on the stack (if any)
; 2. Call the function
; 3. Set up the stack frame (usefull, even if there are no locals)
; 4. Execute the function body
; 5. Store the result in the return register
; 6. Clean up the stack frame
; 7. Return from the function
; 8. Clean up the function arguments
; 9. Load the return register

; normal calling convention is easy but has a lot of overhead. this example
; computes the weighted sum of 4 arguments. The weights are local to the
; function
call_normal:
  ; set up the stack frame, we need 4 local variables
  link 4

  ; now we are ready to compute 4*a1 + 3*a2 + 2*a3 + 1*a4

  ; first 4 * a1
  ldc  4
  ldl -5 ; The argument to ldl is relative to the MP. "MP - 1" is the return
         ; address. Because we have 4 local variables and want the first one,
         ; we are looking for "MP - 1 - 4" which is -5.
  mul
  stl 1  ; store in local variable 1. Once again, this number is relative to the
         ; MP. "MP + 1" is the address of the first local.

  ; repeat this process for the other variables
  ldc  3 ; 3 * a2
  ldl -4
  mul
  stl  2

  ldc  2 ; 2 * a3
  ldl -3
  mul
  stl  3

  ldc  1 ; 1 * a4
  ldl -2
  mul
  stl  4

  ; now we want to do the sum. We can load all locals on the stack in one go
  ; using ldml (Load Multiple Local). We want to start from the first one, and
  ; we want 4 in total
  ldml 1 4

  ; Add the four values together.
  add
  add
  add

  ; store the result in the return register
  str RR

  ; clean up the stack frame
  unlink

  ; return execution to to the address currently pointed to by the SP
  ret

main:
  ; here we call the normal function with arguments 2, 2, 2, 2 or
  ; call_normal(2,2,2,2) in c notation

  ; set up the arguments
  ldc 2
  ldc 2
  ldc 2
  ldc 2

  ; call the function
  bsr call_normal
  ; alternative call:
  ; ldc call_normal
  ; jsr

  ; clean up the arguments
  ajs -4

  ; load the result
  ldr RR

  ; print the result
  trap 0
  halt
