; trap examples
;
; each subroutine performs another trap. Pay close attention to what it does on
; the simulator. Like real programming environments, programs will crash if you
; write to or read from non-existing or closed file descriptors.
;
; Note also that creating a file requires opening in write mode.
;
; Throughout this file we have a couple of functions as to have clear sections.
; To access these functions we use the normal calling convention.
bra cont

; Prints '100' to stdout. Demonstrates 'show integer' trap
printhundred:
  link 0
  ldc 100
  trap 0
  unlink
  ret

; Prints '@' to stdout. Demonstrates 'show character' trap.
printat:
  link 0
  ldc 64   ; print '@' to stdout
  trap 1
  unlink
  ret

; Ask for an integer, put it on the stack, ignore it, and return. Demonstrates
; integer input trap
askint:
  link 0
  trap 10
  ajs -1 ; ignore
  unlink
  ret

; Ask for a char, put it on the stack, ignore it, and return. Demonstrates
; single byte I/O trap
askchr:
  link 0
  trap 11
  ajs -1 ; ignore
  unlink
  ret

; Ask for a string, put it on the stack, ignore it, and return. Demonstrates
; string I/O trap
askstr:
  link 0
  trap 12

  ldr MP
  str SP ; ignore

  unlink
  ret

; Close a file. The only argument is the file descriptor. Note that IO buffers
; will probably only be flushed once the file has been closed properly.
closefile:
  link 0
  ldl -2  ; argument 1 is the fd
  trap 24
  unlink
  ret

; Open a file for reading. We assume that the file name is 5 characters long,
; including the null terminator
openfile_r:
  link 0
  ldml -6 5
  trap 20
  str RR
  unlink
  ret

; Opens a file for writing. We assume that the file name is 5 characters long,
; including the null terminator
openfile_w:
  link 0
  ldml -6 5
  trap 21
  str RR
  unlink
  ret

; fwritec writes one character to a file. The arguments are the file descriptor
; (fd) and the character.
fwritec:
  link 0
  ldl -2 ; fd
  ldl -3 ; character
  trap 24
  unlink
  ret

; fgetc reads one character from a file. the only argument is the file
; descriptor (fd)
fgetc:
  link 0
  ldl -2
  trap 22 ; read a character from the file
  str RR
  unlink
  ret

main:
  bsr printhundred ; demonstrates trap 0
  bsr printat      ; demonstrates trap 1
  bsr askint       ; demonstrates trap 10
  bsr askchr       ; demonstrates trap 11
  bsr askstr       ; demonstrates trap 12

  ; FILE IO
cont:
  ; Write a file name on the stack. File is called "test"
  ldc 0   ; \NULL
  ldc 116 ; t
  ldc 115 ; s
  ldc 101 ; e
  ldc 116 ; t

  ; READING FROM FILES

  ; Open the file again, but for reading. Read 3 characters. Ad-hoc open assumes
  ; 5 character file name
  bsr openfile_r
  ldr RR

  bsr fgetc
  ldr RR    ; load the read character. Remember we need to have the fd on top,
  swp       ; so we swap the top two positions of the stack

  bsr fgetc
  ldr RR
  swp

  bsr fgetc
  ldr RR
  swp

  ; Close the file
  bsr closefile

  ; discard the result
  ajs -4

  ; WRITING TO FILES

  ; Open the file for writing and write "abc" to the file. Ad-hoc open assumes 5
  ; character file name
  bsr openfile_w
  ldr RR

  ldc 97
  bsr fwritec
  swp

  bsr closefile

  ldc 98
  bsr fwritec
  swp

  ldc 99
  bsr fwritec
  swp

  ; Close the file
  bsr closefile

  halt

