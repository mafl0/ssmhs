{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE TypeOperators      #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE DeriveGeneric      #-}

module Main where

import           Options.Generic
import           Control.Monad.Reader
import qualified Data.Text as T

import           SSM.Compiler
import           SSM.Instructions

{- The main SSM interpreter binary
 -
 - Run like: ssmi [options] <filename>
 -
 - options:
 -   --file     -f     file to run
 -   --verbose  -v     dump memory during execution
 -   --memory   -m     memory size, default is 4K
 -   --heapsize -h     heap size, default is 2K
 -}

data Opts w = Opts
  { file     :: w ::: String    <?> "File to run."
  , verbose  :: w ::: Bool      <?> "Dump memory during execution and wait for input after every step."
  , memory   :: w ::: Maybe Int <?> "Total memory size, default is 4K."
  , heapsize :: w ::: Maybe Int <?> "Heap memory size, default is 2K."
  } deriving (Generic)

instance ParseRecord   (Opts Wrapped)
instance ParseRecord   (Opts Unwrapped)
deriving instance Show (Opts Unwrapped)

description :: Text
description = T.pack $ unlines
  [ "Simple Stack Machine Interpreter (SSMi)"
  , "Original idea by Atze Dijkstra (2000). Haskell port by Martijn Fleuren (2019)"
  , ""
  , "ssmi is a simple program that packages a parser, assembler and interpreter. The"
  , "original java interpreter comes with a graphical user interface and the"
  , "possibility of single stepping backward and forward through a program. ssmi does"
  , "not come with an integrated debugger. A debugger is being developed (ssmdb)"
  , "which is based on the gdb interactive shell."
  ]

verboseMsg :: Bool -> String -> IO ()
verboseMsg True msg = putStrLn msg
verboseMsg _    _   = return ()

parseOpts :: Opts Unwrapped -> IO ()
parseOpts (Opts fp v _ _) =
  do
    verboseMsg v $ "Running program " ++ fp
    p <- compileIO v fp

    -- Run the program in the SSM monad. If Execution fails somewhere between
    -- 'interpreting' and 'finished' then the program that was fed to the
    -- interpreter is faulty.
    verboseMsg v "Interpreting ..."
    runReaderT executeSSM p
    verboseMsg v "Finished."

main :: IO ()
main =
  do
    (opts, _) <- unwrapWithHelp description :: IO (Opts Unwrapped, IO ())
    parseOpts opts


