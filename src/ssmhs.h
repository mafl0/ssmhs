/**
 * This header file _simply_ defines (or leaves out) CPP directives that leave
 * sections in the source code about debugging
 */

-- If DEBUGGING is not defined, than no execution details will be given
#define DEBUG
