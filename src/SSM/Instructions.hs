#include "../ssmhs.h"

module SSM.Instructions
  -- Types
  ( InstructionDecodableBy(..)
  , Opcode
  , InstructionCode
  , Instruction(..)

  -- Instruction core
  , instructions
  , instructionNames

  -- Execution table
  , executionTable
  , executeSSM

  -- Debug
#ifdef DEBUG
  , executeSSMDebug
  , executeSSMAutoDebug
#endif

  ) where

import           SSM.Instructions.Core
import           SSM.Instructions.ExecutionTable
import           SSM.Instructions.Execute

