{- The SSM compiler is limited because its basic functionality is just a code
 - generator i.e. it translates the SSM-assembly into a Memory datatype (which
 - is analogous for object code)
 -
 - I have some proposals about what we can include here:
 -
 -  * LLVM backend so the SSM becomes targetable for higher level languages
 -  * Sanity checks on the program
 -    + No undefined symbols being used
 -    + No out-of-bounds memory being addressed
 -  * Optimisations (LLVM can do this if we have a backend for it)
 -
 -}

module SSM.Compiler
  ( assemble
  , assembleIO

  -- Compile a program, or multiple programs into a single executable
  , compileIO
  , compileMIO
  ) where

import           Data.Either
import           Control.Monad

import           SSM.Compiler.Assembler
import           SSM.Memory
import           SSM.Parser

-- Compile a single file to an executable. This is the default for running
-- files.
compileIO :: Bool -> FilePath -> IO Memory
compileIO v fp =
  do
    when v (putStr $ "Reading input from " ++ fp ++ "... ")
    content <- readFile fp
    when v (putStrLn "done.")

    when v (putStr "Parsing assembly...")
    case parse content of
      Left error_ -> fail (show error_)
      Right asm   ->
        do
          when v (putStrLn "done.")
          assembleIO v asm

{- Compile multiple files to a single executable. This is the `linker' that
 - allows multiple programs from different files to use functions from each
 - other.
 -
 - TODO: This is a dumb function that needs some more work.
 -}
compileMIO :: [FilePath] -> IO Memory
compileMIO filepaths =
  do
    -- We assume that parsing succeeds for all files
    files <- mapM readFile filepaths
    let asm = assemble . concat . fmap (fromRight [] . parse) $ files
    initMemory asm

