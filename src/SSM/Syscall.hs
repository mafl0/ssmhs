module SSM.Syscall where

import           Control.Monad.Reader
import           Data.Char
import           System.IO
import           Unsafe.Coerce

import           SSM.Types
import           SSM.Memory
import           SSM.Datatypes.Interpret

-- This function will write information about the requested trap to stderr so
-- that the user has some idea of what is expected of them.
trapInfo :: Int -> SSM ()
trapInfo n =
  liftIO (hPutStr stderr ("trap " ++ show n ++ ": "))

-- Trap to environment function. Invokes a systemcall determined by its
-- argument.
trap :: Int -> SSM ()
trap c =
  let readNullTerminated :: SSM String
      readNullTerminated = do
        sp        <- loadRegister "SP"
        stackMem  <- forM [sp, sp-1 .. 0] (\addr -> readMemory addr)

        let string = takeWhile (/=0) stackMem
            amount = fromIntegral . length $ string

        storeRegister "SP" (sp - amount + 1) -- because the null character counts!

        return (fmap (chr . fromIntegral) string)
  in (>>) (trapInfo c) $ case c of
    -- Pop the topmost element from the stack and print it as an integer.
    0 -> do
      e <- pop
      liftIO (print e)

    -- Pop the topmost element from the stack and print it as a unicode
    -- character.
    1 -> do
      e <- pop
      liftIO $ print (chr . fromIntegral $ e)

    -- Ask the user for an integer input and push it on the stack.
    10 -> do
      e <- liftIO (readLn :: IO SSMWord)
      push e

    -- Ask the user for a unicode character input and push it on the stack.
    11 -> do
      e <- liftIO $ getChar
      push (fromIntegral $ ord e)

    -- Ask the user for a sequence of unicode characters input and push the
    -- characters on the stack terminated by a null-character.
    12 -> do
      e <- liftIO getLine
      mapM_ (push . fromIntegral . ord) $ '\0':reverse e

    -- Pop a null-terminated file name from the stack, open the file for reading
    -- and push a file pointer on the stack.
    20 -> do
      file <- readNullTerminated
      fp   <- liftIO $ openFile file ReadMode
      let fp_ = unsafeCoerce fp :: SSMWord
      push fp_

    -- Pop a null-terminated file name from the stack, open the file for writing
    -- and push a file pointer on the stack.
    21 -> do
      file <- readNullTerminated
      fp   <- liftIO $ openFile file WriteMode
      let fp_ = unsafeCoerce fp :: SSMWord
      push fp_

    -- Pop a file pointer from the stack, read a character from the file pointed
    -- to by the file pointer and push the character on the stack.
    22 -> do
      sp  <- loadRegister "SP"
      fp_ <- pop
      let fp = unsafeCoerce fp_ :: Handle
      c <- liftIO (hGetChar fp)
      push (fromIntegral . ord $ c)

    -- Pop a character and a file pointer from the stack, write the character to
    -- the file pointed to by the file pointer.
    23 -> do
      sp  <- loadRegister "SP"
      c   <- pop
      fp_ <- pop
      let fp = unsafeCoerce fp_ :: Handle
      liftIO $ hPutChar fp (chr . fromIntegral $ c)

    -- Pop a file pointer from the stack and close the corresponding file.
    24 -> do
      fp <- pop
      liftIO (hClose (unsafeCoerce fp :: Handle))

