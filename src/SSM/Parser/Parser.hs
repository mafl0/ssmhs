module SSM.Parser.Parser where

import           Text.Parsec
import           Text.Parsec.Pos

import           SSM.Parser.Lexer
import           SSM.Parser.AST

-- `local' alias for this file
type Parser = Parsec [Token] ()

parseAsm :: [Token] -> Either ParseError Program
parseAsm = Text.Parsec.parse pProgram "SSMasm parser"

pProgram :: Parser Program
pProgram =
  many $ choice
    [     pInstruction
    ,     pRegister
    , try pLabel
    ,     pReference
    ,     pNumber
    ]

tsatisfy :: (Token -> Bool) -> Parser Token
tsatisfy p =
  let f x = if (p x) then Just x else Nothing
  in token show (const $ newPos "" 0 0) f

pInstruction :: Parser SSMasm
pInstruction =
  do
    T_Instruction i <- tsatisfy tIsInstruction
    return (SSM_Instruction i)

pLabel :: Parser SSMasm
pLabel =
  do
    T_Label l <- tsatisfy tIsLabel
    _         <- tsatisfy tIsColon
    return (SSM_Label l)

pReference :: Parser SSMasm
pReference =
  do
    T_Label l <- tsatisfy tIsLabel
    return (SSM_Reference l)

pNumber :: Parser SSMasm
pNumber =
  do
    T_Number n <- tsatisfy tIsNumber
    return (SSM_Number n)

pRegister :: Parser SSMasm
pRegister =
  do
    T_Register r <- tsatisfy tIsRegister
    return (SSM_Register r)
