module SSM.Parser.Lexer
  ( TProgram
  , Token(..)

  -- Predicates over tokens
  , tIsInstruction
  , tIsNumber
  , tIsLabel
  , tIsColon
  , tIsRegister

  , scanTokens
  ) where

import           Data.List
import           Text.Parsec
import           Text.Parsec.Char
import qualified Data.Map as M
import           Data.Char

import           SSM.Types
import           SSM.Parser.Number
import           SSM.Parser.Char
import           SSM.Instructions.Core
import           SSM.Memory.Register

{-
 - EBNF Syntax
 -
 - File         ::= Line* EOF
 - Line         ::= Whitespace* ("" | TokenLine | LabelLine | Comment) EOL
 -
 - TokenLine    ::= Instruction ArgumentList IgnoreEnd
 - LabelLine    ::= Label Whitespace* ":" IgnoreEnd
 - IgnoreEnd    ::= Whitespace* Comment?
 - Comment      ::= (";" | "//") all char except newline
 -
 - Instruction  ::= "ldc" | "bra" | "stl" ...
 - Label        ::= Identifier
 - ArgumentList ::= Whitespace+ Argument ArgumentList | ""
 - Argument     ::= Number | Reference | Register
 - Number       ::= Bin | Oct | Hex | Dec
 - Reference    ::= Identifier
 - Register     ::= "PC" | "R0" | "SP" | "R1" ...
 -
 - Identifier   ::= [a-zA-Z-0-9_.]+
 - Whitespace   ::= ' ' | '\t'
 - EOL          ::= "\n" | "\r\n"
 -
 -}

-- Local parser definition
type Parser = Parsec String ()

-- A tokenised program
type TProgram = [Token]

data Token
  = T_Instruction String -- Instructions
  | T_Number SSMWord     -- Numeric literals
  | T_Label String       -- Labels
  | T_Colon              -- ':'
  | T_Register String    -- Register name
  | T_String String      -- String literal
  deriving Show

-- Predicates

tIsInstruction :: Token -> Bool
tIsInstruction (T_Instruction _) = True
tIsInstruction _                 = False

tIsNumber :: Token -> Bool
tIsNumber (T_Number _) = True
tIsNumber _            = False

tIsLabel :: Token -> Bool
tIsLabel (T_Label _) = True
tIsLabel _           = False

tIsColon :: Token -> Bool
tIsColon (T_Colon) = True
tIsColon _         = False

tIsRegister :: Token -> Bool
tIsRegister (T_Register _) = True
tIsRegister _              = False

-- Lexer
scanTokens :: String -> Either ParseError TProgram
scanTokens = parse lTokens "SSMasm lexer"

lTokens :: Parser [Token]
lTokens = concat <$> (lLine `endBy1` newline) <* eof

-- A single source line
lLine :: Parser [Token]
lLine =
  do
    lWhitespace
    res <- choice [ lComment
                  , try lLabelLine
                  , try lTokenLineWithStringLiteral
                  , lTokenLine
                  , [] <$ string ""
                  ]
    return res

lComment :: Parser [Token]
lComment =
  do
    choice
      [ string ";"
      , string "//"
      ]
    manyTill anyChar (lookAhead (oneOf "\r\n"))
    return []

lTokenLineWithStringLiteral :: Parser [Token]
lTokenLineWithStringLiteral =
  do
    instr        <- lInstruction
    lWhitespace1
    T_String str <- lString
    let nullTerminatedStr = '\0' : reverse str
    ignoreEnd
    return $
      concatMap (\c -> [instr, T_Number (fromIntegral . ord $ c)]) nullTerminatedStr

lTokenLine :: Parser [Token]
lTokenLine =
  do
    instr <- lInstruction
    args  <- option [] lArgumentList
    ignoreEnd
    return (instr:args)

lArgumentList :: Parser [Token]
lArgumentList =
  let
    lArgument =
      choice
        [     lNumber
        ,     lString
        , try lRegister
        ,     lLabel
        ]
  in do
    lWhitespace1
    args <- lArgument `endBy` lWhitespace
    return args

lLabelLine :: Parser [Token]
lLabelLine =
  do
    l <- lLabel
    lWhitespace
    char ':'
    ignoreEnd
    return [l, T_Colon]

-- Keywords: Instructions, Labels and registers
lInstruction :: Parser Token
lInstruction = T_Instruction <$> chooseKeyword (M.keys instructionNames)

lLabel :: Parser Token
lLabel = T_Label <$> lIdentifier

lIdentifier :: Parser String
lIdentifier = many1 (choice [alphaNum, oneOf "_."])

lRegister :: Parser Token
lRegister =
  do
    r <- chooseKeyword (M.keys registersByName)
    notFollowedBy lIdentifier
    return (T_Register r)

-- Numbers
lNumber :: Parser Token
lNumber =
  T_Number <$> choice
    [ try pBin
    , try pOct
    , try pHex
    , pDec
    , pChar
    ]

-- Special
lString :: Parser Token
lString =
  do
    char '\"'
    str <- many (noneOf "\"")
    char '\"'
    return (T_String str)

-- Utility
ignoreEnd :: Parser ()
ignoreEnd =
  do
    lWhitespace
    optional lComment

whitespace :: Parser Char
whitespace = oneOf " \t"

lWhitespace :: Parser ()
lWhitespace = skipMany whitespace

lWhitespace1 :: Parser ()
lWhitespace1 = skipMany1 whitespace

-- We sort on length, longest first, because short strings might be a common
-- prefix of the longer keywords so we want to test them last.
chooseKeyword :: [String] -> Parser String
chooseKeyword = choice . fmap (try . string) . reverse . sortOn length
