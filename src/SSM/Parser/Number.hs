module SSM.Parser.Number
  ( pBin
  , pOct
  , pDec
  , pHex
  --, pFloat
  )where

import qualified Data.Map as M
import           Data.Map (Map)
import           Data.List
import           Data.Maybe
import           Data.Bits
import           Text.Parsec

import           SSM.Types

type Parser = Parsec String ()

pBase :: Char -> Int -> Map Char Int -> Parser SSMWord
pBase c base mapping =
  let allowedChars = fmap char (M.keys mapping)
  in do
    _ <- char '0'
    _ <- char c
    n <- many1 (choice allowedChars)
    return (toIntValue base mapping n)

-- Compute values of numbers
toIntValue :: Int -> Map Char Int -> String -> SSMWord
toIntValue base mapping str =
  let parseBaseChar c = fromMaybe (error "No parse") (M.lookup c mapping)
  in fromIntegral $ foldl' (\x next -> base * x + parseBaseChar next) 0 str

-- Binary Numbers
binMap :: Map Char Int
binMap = M.fromList
  [ ('0',  0)
  , ('1',  1)
  ]

pBin :: Parser SSMWord
pBin = pBase 'b' 2 binMap

-- Octal Numbers
octMap :: Map Char Int
octMap = M.fromList
  [ ('0',  0)
  , ('1',  1)
  , ('2',  2)
  , ('3',  3)
  , ('4',  4)
  , ('5',  5)
  , ('6',  6)
  , ('7',  7)
  ]

pOct :: Parser SSMWord
pOct = pBase 'o' 8 octMap

-- Decimal Numbers

-- Parser, prefixes are ignored for decimal numbers
pDec :: Parser SSMWord
pDec =
  let
    posDec =
      do
        n <- many1 digit
        return (toIntValue 10 hexMap n) -- We just re-use the hexmap
    negDec =
      do
        _ <- char '-'
        n <- posDec
        return (complement n + 1) -- Binary negation
  in choice [posDec, negDec]

-- Hexadecimal Numbers
hexMap :: Map Char Int
hexMap = M.fromList
  [ ('0',  0)
  , ('1',  1)
  , ('2',  2)
  , ('3',  3)
  , ('4',  4)
  , ('5',  5)
  , ('6',  6)
  , ('7',  7)
  , ('8',  8)
  , ('9',  9)
  , ('a', 10) , ('A', 10)
  , ('b', 11) , ('B', 11)
  , ('c', 12) , ('C', 12)
  , ('d', 13) , ('D', 13)
  , ('e', 14) , ('E', 14)
  , ('f', 15) , ('F', 15)
  ]

pHex :: Parser SSMWord
pHex = pBase 'x' 16 hexMap
