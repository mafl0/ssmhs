module SSM.Parser.Char
  ( pChar
  ) where

import           Text.Parsec
import           Data.Char

import           SSM.Types

type Parser = Parsec String ()

pChar :: Parser SSMWord
pChar =
  do
    char '\''
    c <- anyChar
    char '\''
    return (fromIntegral . ord $ c)
