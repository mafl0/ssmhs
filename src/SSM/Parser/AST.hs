module SSM.Parser.AST
  ( Program
  , SSMasm(..)
  -- Predicates

  , isInstruction
  , isReference
  , isLabel
  , isNumber
  , isRegister
  ) where

import           SSM.Types

type Program = [SSMasm]

data SSMasm
  = SSM_Instruction String  -- Instructions
  | SSM_Reference   String  -- Jump references, resolve to addresses
  | SSM_Label       String  -- Labels are points to jump to
  | SSM_Number      SSMWord -- Simple numbers or addresses
  | SSM_Register    String  -- Register names

instance Show SSMasm where
  show y =
    case y of
      SSM_Instruction x -> x
      SSM_Reference   x -> '#' : x
      SSM_Label       x -> x ++ ":"
      SSM_Number      x -> '$' : show x
      SSM_Register    x -> '%' : x

-- Predicates


isInstruction :: SSMasm -> Bool
isInstruction (SSM_Instruction _) = True
isInstruction _                   = False

isReference :: SSMasm -> Bool
isReference (SSM_Reference _) = True
isReference _                 = False

isLabel :: SSMasm -> Bool
isLabel (SSM_Label _) = True
isLabel _             = False

isNumber :: SSMasm -> Bool
isNumber (SSM_Number _) = True
isNumber _              = False

isRegister :: SSMasm -> Bool
isRegister (SSM_Register _) = True
isRegister _                = False

