module SSM.Compiler.Assembler where

import           Control.Arrow
import           Control.Monad
import qualified Data.Map as M
import           Data.Map (Map)
import           Data.List
import           Data.Maybe

import           SSM.Parser.AST
import           SSM.Types
import           SSM.Datatypes.Show
import           SSM.Instructions.Core
import           SSM.Memory

assembleIO :: Bool -> Program -> IO Memory
assembleIO v p =
  do
    when v (putStr "Assembling...")
    a <- (initMemory . assemble) p
    when v (putStrLn "done.")
    return a

assemble :: Program -> BinaryProgram
assemble =
  toBinary . filter (not . isLabel . snd) . resolve . addressAsm

type AddressedAssembly = (Address, SSMasm)
type Binary            = (Address, SSMWord)

type AddressedProgram = [AddressedAssembly]
type BinaryProgram    = [Binary]

-- Hand out addresses to each unit of the program. In principle we want each
-- unit to have a unique address. The reason we cannot simply do "zip [0..] p"
-- is because we do not start at 0, but at @memoryMapSize@, and we want our
-- labels to have the address of the instructions that directly follows it, so
-- we do not wish to increase the address counter in that case
addressAsm :: Program -> AddressedProgram
addressAsm p = go p memoryMapSize []
  where
    -- We are done
    go []                       _   acc = acc
    -- We encountered a label, so we simply pass the counter on without increment
    go ((SSM_Label l):xs)       ctr acc =
      let a = (ctr, (SSM_Label l))
      in go xs ctr (a:acc)
    -- Normal case, this could have been achieved with a zip.
    go (x:xs) ctr acc = go xs (succ ctr) ((ctr, x):acc)

{- Resolve labels. This means that all labels should be replaced with the
 - addresses that they point to. For example:
 -
 - Program:       | Addressed program      | Resolved program | Executable
 - ---------------+------------------------+------------------+---------------
 - main: ldc 1    | main: 0x000: ldc 0x001 | 0x000: ldc 0x001 | 0x000: 0x0b0 0x001
 -       ldc main |       0x002: ldc main  | 0x002: ldc 0x000 | 0x002: 0x0b0 0x000
 -       jsr      |       0x004: jsr       | 0x004: jsr       | 0x004: 0x078
 -
 - We resolve labels by first gathering the symbols from the program. When we
 - have the symbol table we will search through the program for any occurences
 - of labels in the arguments of instructions, which will then be replaced by
 - address pointers. Finally, we remove all assembly that is not an instruction.
 -}
resolve :: AddressedProgram -> AddressedProgram
resolve p =
  let symbolTable = gatherSymbols p

      err_ = error "resolve: Failed because the symbol is not present in the symbol table"

      resolveAsm (SSM_Reference r) = SSM_Number $ fromMaybe err_ (M.lookup r symbolTable)
      resolveAsm x                 = x
  in foldl' (\res (a, next) -> (a, resolveAsm next) : res) [] p

-- Gather all the symbols in the program and construct a mapping to the
-- addresses. This will be our symbol table
gatherSymbols :: AddressedProgram -> Map String Address
gatherSymbols p =
    M.fromList [(name, addr) | (addr, SSM_Label name) <- p]

toBinary :: AddressedProgram -> BinaryProgram
toBinary p =
  let
    -- Unwrapped assembly, there are no more references because we have resolved
    -- them. There are no more labels, because we have deleted them.
    unwrapAssembly asm =
      case asm of
        SSM_Instruction i -> instrcode $ decode i
        SSM_Number x      -> x
        SSM_Register n    -> registerAddress n
        _                 -> error "unwrapAssembly: Encountered non-binarisable assembly while unwrapping. This should never happen."
  in fmap (second unwrapAssembly) p

-- Formatting

-- Utility function for if we want to dump objects somewhere in the future
formatSymbolTable :: Map String Address -> String
formatSymbolTable mapping =
  let symbols = [ (length name, [name, showHex address])
                | (name, address) <- M.toList mapping
                ]
      maxWidth = 1 + maximum (fmap fst symbols)
      fmtSym   = \x n a -> concat [n, replicate (maxWidth - x) ' ', a]
  in unlines $ fmap (\(x, [n, a]) -> fmtSym x n a) symbols

{- Formats an assembled program.
 - e.g. a program of the form [bra, #main, main:, ldc, $0, ldc, $1, halt]
 - will become
 -
 - "bra #main\n
 -  main:\n
 -  ldc $0\n
 -  ldc $1\n
 -  halt"
 -
 - for pretty printing
 -}
formatAssembledProgram :: AddressedProgram -> String
formatAssembledProgram [] = ""
formatAssembledProgram p  =
  let ((addr, is), rest) = takeInstruction p
      line               = showHex addr ++ " " ++  (unwords $ fmap show is)
  in line ++ "\n" ++ formatAssembledProgram rest

-- Helper function to take the next instruction and it's arguments from the head
-- of the list. We assume that there is always an instruction on the head.
takeInstruction :: [(a, SSMasm)] -> ((a, [SSMasm]), [(a, SSMasm)])
takeInstruction []                                 = error "takeInstruction: empty list"
takeInstruction ((addr, i@(SSM_Instruction instr)):xs) =
  let instructionDetails = decode instr
      arguments   = snd <$> take (instrSize instructionDetails) xs
      rest        = drop (instrSize instructionDetails) xs
  in ((addr, i : arguments), rest)
takeInstruction _ = error "takeInstruction: No instruction at the head of the program"


