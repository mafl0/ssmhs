module SSM.Debugger.Stack where

import           Data.Array (Array)
import qualified Data.Array as A
import           Control.Monad.Reader
import           Data.Array.IO
import           Text.Printf

import           SSM.Memory
import           SSM.Types

dumpStackSSM :: SSM ()
dumpStackSSM =
  do
    mem <- ask
    liftIO (dumpStackIO mem)

dumpStackIO :: Memory -> IO ()
dumpStackIO mem =
  do
    sp  <- readMemory undefined -- TODO
    lst <- A.assocs <$> freeze mem
    let formatted = formatStackDump .
                    filter (\(addr, _) -> sp < addr
                                       && addr < heapStart) $ lst
    putStrLn formatted

formatStackDump :: [(Address, SSMWord)] -> String
formatStackDump =
  unlines . fmap (uncurry (printf "% 8x\t% 8x"))
