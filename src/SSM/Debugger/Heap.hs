module SSM.Debugger.Heap where

import           Control.Monad.Reader

import           SSM.Memory
import           SSM.Types
import           SSM.Debugger.HexDump

dumpHeapIO :: Maybe Address -> Maybe Address -> Memory -> IO ()
dumpHeapIO mLowAddress =
  let mLowAddress' = (+(heapSize-1)) <$> mLowAddress
  in hexdumpIO mLowAddress'

dumpHeapSSM :: Maybe Address -> Maybe Address -> SSM ()
dumpHeapSSM mLowAddress mHighAddress =
  do
    mem <- ask
    liftIO (dumpHeapIO mLowAddress mHighAddress mem)
