module SSM.Debugger.HexDump
  ( hexdumpIO
  , hexdumpSSM
  ) where

import           Control.Monad.Reader
import           Data.Array (Array)
import qualified Data.Array as A
import           Data.Array.IO
import           Text.Printf
import           Data.Maybe

import           SSM.Types
import           SSM.Memory

type MemoryList   = [(Address, SSMWord)]
type MemoryFrozen = Array Address SSMWord

-- Dump the memory from a lower bound up to an upper bound. If the lower bound
-- is Nothing then 0 will be assumed. If the high bound is Nothing the amount
-- of memory will be assumed.
hexdumpIO :: Maybe Address -> Maybe Address -> Memory -> IO ()
hexdumpIO mLowAddress mHighAddress mem =
  let lowAddress  = fromMaybe 0 mLowAddress
      highAddress = fromMaybe (amountOfMemory-1) mHighAddress
  in do
    guard (lowAddress < highAddress)         -- Makes no sense
    guard (0 <= lowAddress)                  -- No negative addresses
    guard (highAddress <= amountOfMemory-1)  -- No addresses that do not exist

    memf <- freeze mem
    putStrLn . formatHexDump . memRegionToList lowAddress highAddress $ memf

hexdumpSSM :: Maybe Address -> Maybe Address -> SSM ()
hexdumpSSM mLowAddress mHighAddress =
  do
    mem <- ask
    liftIO (hexdumpIO mLowAddress mHighAddress mem)

formatHexDump :: MemoryList -> String
formatHexDump =
  let showLine (addr, values) = printf "% 8x: " addr ++ (unwords $ fmap (printf "% 8x") values)
      toAddressedLine ((addr, val):xs) = (addr, val : fmap snd xs)
  in unlines . fmap (showLine . toAddressedLine) . groups 8

-- Convert a memory region to a list
memRegionToList :: Address -> Address -> MemoryFrozen -> MemoryList
memRegionToList lowAddr highAddr =
  filter (\(addr, _) -> lowAddr <= addr && addr <= highAddr) . A.assocs

-- Group a list of things into a list of lists-of-things. The inner list is
-- limited in size by the first parameter
groups :: Int -> [a] -> [[a]]
groups _ [] = []
groups n xs =
  let (y, ys) = splitAt n xs
  in y : groups n ys
