module SSM.Debugger.Shell where

data Action
  = Step
  | Break
  | Abandon
  | Print Printable
  | Nop
  | Help

data Printable
  = Register
  | Stack
  | Heap
  | MemoryRaw

replInit :: IO ()
replInit =
  do
    putStrLn . concat $
      [ "Does not support nice line editing. Run this program in \'rlwrap\'"
      , "to get that functionality\n"
      ]
    repl

-- Read-Evaluate-Print-Loop
repl :: IO ()
repl =
  do
    putStr "(SSMdb) "
    -- Read
    command <- getLine

    -- Evaluate-Print
    case parse command of
      Step    -> return ()
      Break   -> return ()
      Abandon -> return ()
      Print x -> return ()
      Help    -> help
      Nop     -> help

    -- Loop
    repl

unknownCommand :: String -> IO ()
unknownCommand cmd = putStrLn $ "Unknown command: " ++ cmd

help :: IO ()
help = putStrLn helpMsg
  where
    helpMsg =
      unlines
        [ "SSMdb debugger help:"
        , ""
        , "# print:"
        , "With print you can ask to print memory contents. The options for"
        , "printing are:"
        , "  * reg <name>    Prints the content of register <name>"
        , "  * stack         Dumps a stack trace"
        , "  * heap <size>   Dumps the heap up to <size>"
        , "  * mem <address> Read a specified memory cell"
        , "break"
        , "help"
        , "abandon"
        ]

parse :: String -> Action
parse _ = Nop
