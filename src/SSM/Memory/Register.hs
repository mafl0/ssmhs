{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards   #-}

module SSM.Memory.Register
  ( RegisterAddressableBy(..)
  , loadRegister
  , storeRegister
  , registers
  , registersByName
  , registersByAddress
  ) where

import           Data.Maybe
import           Data.Map (Map)
import qualified Data.Map as M
import           System.Random

import           SSM.Types
import           SSM.Memory.Core

{- Register file
 -
 - Label      Address     Name              Description
 - ----------------------------------------------------------------------------
 - R0 / PC    0x000       Program counter   next instruction to execute
 - R1 / SP    0x001       Stack pointer     next free address on the stack
 - R2 / MP    0x002       Mark pointer
 - R3 / HP    0x003       Heap pointer      next free address on the heap
 - R4 / RR    0x004       Return register   register that can be used to return
 -                                          values from functions
 - R5         0x005       General purpose   Register to store anything
 - R6         0x006       General purpose   Register to store anything
 - SR / R7    0x007       Status register
 -
 - Special register file
 -
 - Label      Name              Description
 - ----------------------------------------------------------------------------
 - devzero    Zero device       Always returns zero when read, ignores what is
 -                              written
 - devrand    Random device     Holds a random word. When read this value is
 -                              returned. Ignores what is written
 -
 - Note that the register protection is only implemented when using the
 - storeRegister and loadRegister functions to address them. when directly
 - accessing the values in memory they will act as a normal address
 -}

 -- Data Type

data Register = Register
  { regLabel       :: String
  , regAddress     :: SSMWord
  , regIgnoreRead  :: Bool
  , regIgnoreWrite :: Bool
  , regLoadAction  :: Address -> SSM SSMWord
  , regStoreAction :: Address -> SSMWord -> SSM ()
  }

-- Reading

defaultRead :: Address -> SSM SSMWord
defaultRead = readMemory

ignoreRead :: Address -> SSM SSMWord
ignoreRead _ = return 0

randomRead :: Address -> SSM SSMWord
randomRead addr =
  do
    next_ <- liftIO $ randomIO
    cur   <- readMemory addr

    -- Cannot be done with storeRegister because it will be ignored
    writeMemory addr next_

    return cur

-- Writing

defaultWrite :: Address -> SSMWord -> SSM ()
defaultWrite = writeMemory

ignoreWrite :: Address -> SSMWord -> SSM ()
ignoreWrite _ _ = return ()

-- Register definitions
registers :: [Register]
registers =
  --         Name       Address  IgnoreRead  IgnoreWrite ReadAction    WriteAction
  [ Register "R0"       0x000    False       False       defaultRead   defaultWrite
  , Register "PC"       0x000    False       False       defaultRead   defaultWrite
  , Register "R1"       0x001    False       False       defaultRead   defaultWrite
  , Register "SP"       0x001    False       False       defaultRead   defaultWrite
  , Register "R2"       0x002    False       False       defaultRead   defaultWrite
  , Register "MP"       0x002    False       False       defaultRead   defaultWrite
  , Register "R3"       0x003    False       False       defaultRead   defaultWrite
  , Register "HP"       0x003    False       False       defaultRead   defaultWrite
  , Register "R4"       0x004    False       False       defaultRead   defaultWrite
  , Register "RR"       0x004    False       False       defaultRead   defaultWrite
  , Register "R5"       0x005    False       False       defaultRead   defaultWrite
  , Register "R6"       0x006    False       False       defaultRead   defaultWrite
  , Register "R7"       0x007    False       False       defaultRead   defaultWrite
  , Register "SR"       0x007    False       False       defaultRead   defaultWrite
  , Register "devzero"  0x008    False       True        ignoreRead    ignoreWrite
  , Register "devrand"  0x009    False       True        randomRead    ignoreWrite
  ]

-- Maps of registers

registersByName :: Map String Register
registersByName = M.fromList $
  fmap (\r@Register{..} -> (regLabel, r)) registers

-- Todo: Convert this to something else than List. We cannot use Map because
-- there are more keys (such as PC/R0) that address the same address.
registersByAddress :: [(Address, Register)]
registersByAddress =
  fmap (\r@Register{..} -> (regAddress, r)) registers

-- Class of datatypes that a register can be addressed by.
class RegisterAddressableBy a where
  registerAddress :: a -> Address

instance RegisterAddressableBy String where
  registerAddress =
    let errInvalidLabel_ = error "registerAddress: Invalid register label"
    in regAddress . fromMaybe errInvalidLabel_ . flip M.lookup registersByName

instance RegisterAddressableBy Address where
  registerAddress = id

loadRegister :: (Show a, RegisterAddressableBy a) => a -> SSM SSMWord
loadRegister k =
  let errMsg = unwords ["Register", show k, "could not be loaded: Unknown label"]
      addr   = registerAddress k
      reg    = fromMaybe (error errMsg) (lookup addr registersByAddress)
  in regLoadAction reg addr

storeRegister :: (Show a, RegisterAddressableBy a) => a -> SSMWord -> SSM ()
storeRegister k val =
  let errMsg = unwords ["Register", show k, "could not be stored: Unknown label"]
      addr = registerAddress k
      reg  = fromMaybe (error errMsg) (lookup addr registersByAddress)
  in regStoreAction reg addr val


