module SSM.Memory.Core
  ( Memory()
  , SSM
  , SSMVoid
  , memorySize
  , readMemory
  , writeMemory
  , module Control.Monad.Reader
  ) where

import           Control.Monad.Reader
import           Data.Array.IO

import           SSM.Types

{- We have a flat memory model. The program and its stack have to fit in some
 - amount of words, according to the documentation this is 2000 words but i will
 - give 2K of memory by default because it is nicer in base 2.
 -
 - Program starts at 0x008 and ends at S
 - Stack starts at S and ends at 0x7FF
 - Heap starts at 0x800 and ends at inf
 -
 - This means that if the program is too big, there is no stack. And when the
 - proram is >2K words, then the code is in the heap.
 -
 - Registers are also memory mapped and are at the beginning of the address
 - space
 -}
memorySize :: Int
memorySize = 0x800

-- Reader monad that keeps a reference to an array. Note that this is an
-- IOArray so it is a mutable thing. The SSM is basically defined by the state
-- of its memory. So we define it here.
type SSM     = ReaderT Memory IO
type SSMVoid = SSM ()
type Memory  = IOUArray Address SSMWord

readMemory :: Address -> SSM SSMWord
readMemory addr =
  do
    mem <- ask
    liftIO (readArray mem addr)

writeMemory :: Address -> SSMWord -> SSM ()
writeMemory addr val =
  do
    mem <- ask
    liftIO (writeArray mem addr val)
