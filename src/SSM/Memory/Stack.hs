module SSM.Memory.Stack where

import           SSM.Types
import           SSM.Memory.Register
import           SSM.Memory.Core

-- Read the stack pointer from the SP register, read the memory at that
-- address (because the stack pointer points to the current TOP) and then
-- decrement the stack pointer by 1. Return the value that was read.
pop :: SSM SSMWord
pop =
  do
    sp <- loadRegister "SP"
    storeRegister "SP" (sp - 1)
    readMemory sp

-- Read the stack pointer and write to the memory at one address higher.
-- Increment the stack pointer by one.
push :: SSMWord -> SSM ()
push val =
  do
    sp <- loadRegister "SP"
    writeMemory (sp + 1) val
    storeRegister "SP" (sp + 1)
