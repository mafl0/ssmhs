{- The SSM only has one internal data representation which as of now is a 32-bit
 - word. The data can be interpreted as any of the datatypes listed below. This
 - modules purpose is mainly for displaying values to the user and to read input
 - from the user.
 -}

module SSM.Datatypes
  -- Signed
  ( ssmToSInt8
  , ssmToSInt16
  , ssmToSInt32
  , ssmFromSInt8
  , ssmFromSInt16
  , ssmFromSInt32

  -- Unsigned
  , ssmToUInt8
  , ssmToUInt16
  , ssmToUInt32
  , ssmFromUInt8
  , ssmFromUInt16
  , ssmFromUInt32

  -- Char
  , ssmToChar
  , ssmFromChar

  -- Bool
  , ssmToBool
  , ssmFromBool

  -- Float
  , ssmToFloat
  , ssmFromFloat
  ) where

import           SSM.Datatypes.Bool
import           SSM.Datatypes.Int
import           SSM.Datatypes.Float
import           SSM.Datatypes.Char

