#include "../../ssmhs.h"

{- This module contains the execution table. Each instruction code (an integer)
 - is mapped to an action that changes the state of the machine (SSM ()). If you
 - want to add instructions to the machine then you have to add it to
 -
 - SSM.Instructions.Core, for its description, opcode, instructioncode, etc.
 - SSM.Instructions.ExecutionTable, for the specification on how it should
 - change the state of the machine.
 -
 -}

module SSM.Instructions.ExecutionTable where

import           Control.Monad.Reader
import           Data.Array
import           Data.Bits

import           SSM.Instructions.Core
import           SSM.Memory
import           SSM.Types
import           SSM.Datatypes.Interpret
import           SSM.Syscall

{- Retrieve information about hte program counter and arguments to the
 - instruction from the memory. Getting this information is a common pattern in
 - each instruction and can be used as follows:
 -
 - ...
 - do
 -   [pc, arg1, arg2, ... argN] <- pcDetails N
 -}
pcDetails :: Int -> SSM [SSMWord]
pcDetails n =
  do
    pc   <- loadRegister "PC"
    args <- forM [pc+1 .. pc+fromIntegral n] readMemory
    return (pc:args)

-- Advancing the program counter (PC) is a common pattern.
pcAdvance :: SSMWord -> SSM ()
pcAdvance n =
  do
    pc <- loadRegister "PC"
    storeRegister "PC" (pc + n)

-- No-operation
nop :: SSM ()
nop = return ()

-- Unary stack operation
unOp :: (SSMWord -> SSMWord) -> SSM ()
unOp f =
  do
    arg <- pop
    push (f arg)
    pcAdvance 1

-- Binary stack operation
binOp :: (SSMWord -> SSMWord -> SSMWord) -> SSM ()
binOp f =
  do
    arg1 <- pop
    arg2 <- pop
    push (f arg2 arg1)
    pcAdvance 1

-- Definition of each instruction. New instructions should be added here as well
-- as in the SSM.Instructions.Core module
executionTable :: Array InstructionCode SSMVoid
executionTable = array (0x00, 0xFF)
  -- Binary and unary stack operations. These are straight forward.
  [ {- add    -} (0x01, binOp (+))
  , {- sub    -} (0x0C, binOp (-))
  , {- mul    -} (0x08, binOp (*))
  , {- div    -} (0x04, binOp div)
  , {- mod    -} (0x07, binOp mod)
  , {- and    -} (0x02, binOp (.&.))
  , {- or     -} (0x09, binOp (.|.))
  , {- xor    -} (0x0D, binOp xor)
  , {- neg    -} (0x20, unOp negate)
  , {- not    -} (0x21, unOp complement)
  , {- eq     -} (0x0E, binOp (\x y -> asBinary (x == y)))
  , {- ne     -} (0x0F, binOp (\x y -> asBinary (x /= y)))
  , {- lt     -} (0x10, binOp (\x y -> asBinary (x <  y)))
  , {- gt     -} (0x11, binOp (\x y -> asBinary (x >  y)))
  , {- le     -} (0x12, binOp (\x y -> asBinary (x <= y)))
  , {- ge     -} (0x13, binOp (\x y -> asBinary (x >= y)))

  -- Adjust the stack pointer. Adds the inline argument to the stack pointer
  , {- ajs    -} (0x64, do [_, arg]  <- pcDetails 1

                           sp  <- loadRegister "SP"
                           storeRegister "SP" (arg + sp)
                           pcAdvance 2
                 )

  -- Branch always. Jumps to the address specified as inline argument
  , {- bra    -} (0x68, do [_, arg]  <- pcDetails 1
                           storeRegister "PC" arg
                 )

  -- Branch if false. If the top of the stack is 0, then jump to the address
  -- specified as inline argument, otherwise continue.
  , {- brf    -} (0x6C, do [_, arg] <- pcDetails 1
                           val <- pop
                           if val == 0 then
                            storeRegister "PC" arg -- Jump address
                           else
                            pcAdvance 2
                 )

  -- Branch if true, same as false but branch if the top of the stack is not 0
  , {- brt    -} (0x6D, do [_, arg] <- pcDetails 1
                           val <- pop
                           if val /= 0 then
                            storeRegister "PC" arg
                           else
                            pcAdvance 2
                 )

  -- Branch to the subroutine specified as inline argument. Pushes the return
  -- address on the stack.
  , {- bsr    -} (0x70, do [pc, arg] <- pcDetails 1
                           push (pc + 2)               -- Push return addr
                           storeRegister "PC" arg
                 )

  -- Jump to subroutine. Same as BSR but instead takes its argument from
  -- the stack instead of inline.
  , {- jsr    -} (0x78, do pc     <- loadRegister "PC" -- PC
                           srAddr <- pop               -- Stack arg
                           storeRegister "PC" srAddr   -- Jump to arg
                           push (pc + 1)               -- Push return addr
                 )

  -- Return (from subroutine). Pops a value from the stack and jumps to it.
  , {- ret    -} (0xA8, do addr <- pop
                           storeRegister "PC" addr
                 )

  -- Halt execution. This is only here for completion but it should never be
  -- executed. The stepper/handler should make sure that when this instruction
  -- is next that the execution stops.
  , {- halt   -} (0x74, nop)

  -- Link reserves space for local variables.
  -- TODO: describe
  , {- link   -} (0xA0, do [_, arg] <- pcDetails 1
                           mp <- loadRegister "MP"
                           push mp
                           sp <- loadRegister "SP"
                           storeRegister "MP" sp
                           storeRegister "SP" (sp + arg)

                           pcAdvance 2
                 )

  -- Unlink removes space that was reserved for local variables
  -- TODO: describe
  , {- unlink -} (0xCC, do mp <- loadRegister "MP"
                           mp_old <- readMemory mp
                           storeRegister "MP" mp_old
                           storeRegister "SP" (mp - 1)
                           pcAdvance 1
                 )

  -- Push a constant on the stack. The constant is specified inline
  , {- ldc    -} (0x84, do [_, arg]  <- pcDetails 1
                           push arg
                           pcAdvance 2
                 )

  -- Load the contents of a register on the stack. The register is specified
  -- inline
  , {- ldr    -} (0x90, do [_, arg]  <- pcDetails 1

                           val <- loadRegister arg
                           push val
                           pcAdvance 2
                 )

  -- Load register from register. The value held by the register that is
  -- specified by argument 2 is stored in the register specified by argument 1.
  , {- ldrr   -} (0x94, do [_, arg1, arg2] <- pcDetails 2
                           val <- readMemory arg2
                           storeRegister arg1 val
                           pcAdvance 3
                 )

  -- Push a value on the stack. The value is addresses relative to the stack
  -- pointer. e.g. "lds -1" with a stack pointer of 0x9 loads the value of
  -- address 0x8.
  , {- lds    -} (0x98, do [_, arg] <- pcDetails 1
                           sp  <- loadRegister "SP"
                           val <- readMemory (sp + arg)
                           push val
                           pcAdvance 2
                 )

  -- Push a local variable on the stack.
  , {- ldl    -} (0x88, do [_, arg]  <- pcDetails 1

                           mp  <- loadRegister "MP"
                           val <- readMemory (mp + arg)
                           push val
                           pcAdvance 2
                 )

  , {- lda    -} (0x7C, do [_, arg] <- pcDetails 1
                           addr <- pop
                           val <- readMemory (addr + arg)
                           push val
                           pcAdvance 2
                 )

  -- ldh is equivalent to lda?
  , {- ldh    -} (0xD0, do [_, arg] <- pcDetails 1
                           addr <- pop
                           val <- readMemory (addr + arg)
                           push val
                           pcAdvance 2
                 )

  , {- ldsa   -} (0x9C, do [_, arg] <- pcDetails 1
                           sp  <- loadRegister "SP"
                           push (sp + arg)
                           pcAdvance 2
                 )

  , {- ldla   -} (0x8C, do [_, arg] <- pcDetails 1
                           mp <- loadRegister "MP"
                           push (mp + arg)
                           pcAdvance 2
                 )

  , {- ldaa   -} (0x80, do [_, arg] <- pcDetails 1
                           val <- pop
                           push (val + arg)
                           pcAdvance 2
                 )

  , {- ldms   -} (0x9A, do [_, arg1, arg2] <- pcDetails 2
                           sp   <- loadRegister "SP"
                           forM_ [0..arg2-1]
                            (\offs ->
                              do val <- readMemory (sp + arg1 + offs)
                                 push val
                            )
                           pcAdvance 3
                 )

  , {- ldml   -} (0x8A, do [_, arg1, arg2] <- pcDetails 2
                           mp   <- loadRegister "MP"
                           forM_ [0..arg2-1]
                            (\offs ->
                              do val <- readMemory (mp + arg1 + offs)
                                 push val
                            )
                           pcAdvance 3
                 )

  , {- ldma   -} (0x7E, do [_, arg1, arg2] <- pcDetails 2
                           addr <- pop
                           when (arg2 > 0) $ do
                            forM_ [0..arg2-1]
                              (\offs -> do
                                val <- readMemory (addr + arg1 + offs)
                                push val
                              )
                           pcAdvance 3

                 )

    -- Load Multiple from Heap. Pushes values pointed to by the value at the top
    -- of the stack. The pointer value is offset by a constant offset. Same as
    -- single load variant but the second inline parameter is size.
  , {- ldmh   -} (0xD4, do [_, arg1, arg2] <- pcDetails 2
                           addr <- pop
                           when (arg2 > 0) $ do
                            forM_ [1..arg2]
                              (\offs -> do
                                val <- readMemory (addr - arg2 + offs + arg1)
                                push val
                              )
                           pcAdvance 3
                 )

  -- Store a value in a register. In this case we have to advance the program
  -- counter first in case someone writes to the PC, because that write has to
  -- overwrite the auto-increment of the SSM. This is the same behaviour as in
  -- the java applet
  , {- str    -} (0xB4, do [_, arg] <- pcDetails 1
                           pcAdvance 2
                           val   <- pop
                           storeRegister arg val
                 )

  , {- sts    -} (0xB8, do [_, arg] <- pcDetails 1
                           sp <- loadRegister "SP"
                           val <- pop
                           writeMemory (sp + arg) val
                           pcAdvance 2
                 )

  , {- stl    -} (0xB0, do [_, arg] <- pcDetails 1
                           mp  <- loadRegister "MP"
                           val <- pop
                           writeMemory (mp + arg) val
                           pcAdvance 2
                 )

  , {- sta    -} (0xAC, do [_, arg] <- pcDetails 1
                           addr <- pop
                           val  <- pop
                           writeMemory (addr + arg) val
                           pcAdvance 2
                 )
  , {- sth    -} (0xD6, do hp  <- loadRegister "HP"
                           val <- pop
                           writeMemory hp val
                           push hp
                           storeRegister "HP" (hp + 1)
                           pcAdvance 1
                 )

  -- Store multiple into stack. Pops @arg1@ values off the stack and stores them
  -- sequentially in an address relative to the top of the stack
  , {- stms   -} (0xBA, do [_, arg1, arg2] <- pcDetails 2
                           sp <- loadRegister "SP"
                           when (arg2 > 0) $ do
                            values <- replicateM (fromIntegral arg2) pop
                            forM_ (zip [0..arg2-1] (reverse values))
                              (\(offs, val) -> do
                                writeMemory (sp + arg1 + offs) val
                              )
                           pcAdvance 3
                 )

  -- Store multiple into the local variables. Pops @arg2@ values off the stack
  -- and stores them sequentially, relative to the mark pointer. @arg1@ is a
  -- constant offset.
  , {- stml   -} (0xB2, do [_, arg1, arg2] <- pcDetails 2
                           mp <- loadRegister "MP"
                           when (arg2 > 0) $ do
                            forM_ [0..arg2-1]
                              (\offs -> do
                                val <- pop
                                writeMemory (mp + arg1 - 1 + arg2 - offs) val
                              )
                           pcAdvance 3
                 )

  -- Store Multiple via Address. Pops values from the stack and stores it in a
  -- location relative to the value at the top of the stack. Same as single
  -- store variant but second inline parameter is size.
  , {- stma   -} (0xAE, do [_, arg1, arg2] <- pcDetails 2
                           addr <- pop
                           when (arg2 > 0) $ do
                            forM_ [0..arg2-1]
                              (\offs -> do
                                val <- pop
                                writeMemory (addr + arg1 + arg2 - offs - 1) val
                              )
                           pcAdvance 3
                 )

  -- Store multiple into heap. Pops values off the stack and stores them in the
  -- heap. The address of the last value is pushed on the stack. @arg@ is the
  -- size.
  , {- stmh   -} (0xD8, do [_, arg] <- pcDetails 1
                           hp <- loadRegister "HP"
                           when (arg > 0) $ do
                            forM_ [0..arg-1] $
                              (\offs -> do
                                val <- pop
                                writeMemory (hp - offs + arg - 1) val
                              )
                            storeRegister "HP" (hp + arg)
                            push (hp + arg - 1)
                           pcAdvance 2
                 )

  -- No operation. Simply skips.
  , {- nop    -} (0xA4, pcAdvance 1)

  -- Swap the two values at the top of the stack
  , {- swp    -} (0xBC, do val1 <- pop
                           val2 <- pop
                           push val1
                           push val2
                           pcAdvance 1
                 )

  -- Swap with register. Swaps the value at the top of the stack with the value
  -- of a register that is specified inline.
  , {- swpr   -} (0xC0, do [_, arg] <- pcDetails 1
                           regVal <- loadRegister arg
                           stVal  <- pop
                           push regVal
                           storeRegister arg stVal
                           pcAdvance 2
                 )

  -- Swap register with register. Swaps the values of the registers that are
  -- sepcified inline. swprr R5 R6 swaps the value of R5 with the value of R6
  , {- swprr  -} (0xC4, do [_, arg1, arg2] <- pcDetails 2
                           reg1Val <- loadRegister arg1
                           reg2Val <- loadRegister arg2
                           storeRegister arg1 reg2Val
                           storeRegister arg2 reg1Val
                           pcAdvance 3
                 )

  -- Trap implements syscalls. Interrupts the control flow and hands of control
  -- to the environment (kernel) which will handle something before returning
  -- control flow back to the program. We use this for IO.
  , {- trap   -} (0xC8, do [_, arg] <- pcDetails 1
                           trap (fromIntegral arg)
                           pcAdvance 2
                 )
  ]

