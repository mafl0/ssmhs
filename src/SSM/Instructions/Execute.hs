#include "../../ssmhs.h"

module SSM.Instructions.Execute where

import           Text.Printf
import           Control.Monad.Reader
import           Data.Array

-- Conditional imports
#ifdef DEBUG
import           Control.Concurrent
import           SSM.Debugger.HexDump
#endif

import           SSM.Instructions.ExecutionTable
import           SSM.Instructions.Core
import           SSM.Memory
import           SSM.Debugger.Heap

fetch :: SSM InstructionCode
fetch =
  do
    pc <- loadRegister "PC"
    readMemory pc

#ifdef DEBUG
ssmLog :: String -> SSM ()
ssmLog msg = liftIO (putStrLn msg)

-- | Same as executeSSM, but prints information about the execution
executeSSMDebug :: SSM ()
executeSSMDebug =
  do
    ssmLog "Lower Memory:"
    hexdumpSSM (Just 0) (Just 0x50)

    ssmLog "Heap dump:"
    dumpHeapSSM (Just 0) (Just 0x820)
    _ <- liftIO getChar

    ic <- fetch

    ssmLog (printf "Executing instruction % x" ic)

    -- Unless the instruction code is HALT, perform the next step
    unless (ic == 0x74) $ do
      executionTable ! ic
      executeSSMDebug

-- Same as executeSSMDebug, with automatic stepping. t is specified in
-- milliseconds between steps
executeSSMAutoDebug :: Int -> SSM ()
executeSSMAutoDebug t =
  do
    ssmLog "Lower Memory:"
    hexdumpSSM (Just 0) (Just 0x50)

    ssmLog "Heap dump:"
    dumpHeapSSM (Just 0) (Just 0x820)

    liftIO $ threadDelay (t * 1000)

    ic <- fetch

    ssmLog (printf "Executing instruction % x" ic)

    -- Unless the instruction code is HALT, perform the next step
    unless (ic == 0x74) $ do
      executionTable ! ic
      executeSSMAutoDebug t
#endif

-- | Execution function. Fetches the next instruction and looks up in the
-- execution table what SSM-action should be executed. If the instruction is not
-- HALT, then one step is taken, otherwise the function exits.
executeSSM :: SSM ()
executeSSM =
  do
    pc <- loadRegister "PC"
    ic <- readMemory pc

    -- Unless the instruction code is HALT, perform the next step
    unless (ic == 0x74) $ do
      executionTable ! ic
      executeSSM

