{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances    #-}

module SSM.Instructions.Core where

import qualified Data.Map as M
import           Data.Map (Map)
import           Data.Maybe

import           SSM.Memory
import           SSM.Types

type InstructionCode = SSMWord
type Opcode          = String

-- Information about instructions
data Instruction = Instruction
  { opcode     :: Opcode
  , instrcode  :: InstructionCode
  , instrSize  :: Int
  }

instance Show Instruction where
  show (Instruction oc _ _) = oc

-- Instruction set of the SSM. This should be a mapping from instruction
-- codes to instructions
instructions :: Map InstructionCode Instruction
instructions = M.fromList . fmap (\i -> (instrcode i, i)) $ is
  where
    is =
      [ Instruction "add"    0x01 0
      , Instruction "sub"    0x0c 0
      , Instruction "mul"    0x08 0
      , Instruction "div"    0x04 0
      , Instruction "mod"    0x07 0
      , Instruction "and"    0x02 0
      , Instruction "or"     0x09 0
      , Instruction "xor"    0x0d 0
      , Instruction "neg"    0x20 0
      , Instruction "not"    0x21 0
      , Instruction "eq"     0x0e 0
      , Instruction "ne"     0x0f 0
      , Instruction "lt"     0x10 0
      , Instruction "gt"     0x11 0
      , Instruction "le"     0x12 0
      , Instruction "ge"     0x13 0
      , Instruction "ajs"    0x64 1
      , Instruction "bra"    0x68 1
      , Instruction "brf"    0x6c 1
      , Instruction "brt"    0x6d 1
      , Instruction "bsr"    0x70 1
      , Instruction "jsr"    0x78 0
      , Instruction "ret"    0xa8 0
      , Instruction "halt"   0x74 0
      , Instruction "link"   0xa0 1
      , Instruction "unlink" 0xcc 0
      , Instruction "ldc"    0x84 1
      , Instruction "ldr"    0x90 1
      , Instruction "ldrr"   0x94 2
      , Instruction "lds"    0x98 1
      , Instruction "ldl"    0x88 1
      , Instruction "lda"    0x7c 1
      , Instruction "ldh"    0xd0 1
      , Instruction "ldsa"   0x9c 1
      , Instruction "ldla"   0x8c 1
      , Instruction "ldaa"   0x80 1
      , Instruction "ldms"   0x9a 2
      , Instruction "ldml"   0x8a 2
      , Instruction "ldma"   0x7e 2
      , Instruction "ldmh"   0xd4 2
      , Instruction "str"    0xb4 1
      , Instruction "sts"    0xb8 1
      , Instruction "stl"    0xb0 1
      , Instruction "sta"    0xac 1
      , Instruction "sth"    0xd6 0
      , Instruction "stms"   0xba 2
      , Instruction "stml"   0xb2 2
      , Instruction "stma"   0xae 2
      , Instruction "stmh"   0xd8 1
      , Instruction "nop"    0xa4 0
      , Instruction "swp"    0xbc 0
      , Instruction "swpr"   0xc0 1
      , Instruction "swprr"  0xc4 0
      , Instruction "trap"   0xc8 1
      ]

-- Mapping of opcodes to instruction codes. We use this to look up details about
-- instructions during parsing and assembling.
instructionNames :: Map Opcode InstructionCode
instructionNames =
  M.fromList . fmap (\i -> (opcode i, instrcode i)) . M.elems $ instructions

-- Class of types that can decode (get) details about an instruction
class InstructionDecodableBy a where
  decode :: a -> Instruction

-- Instructions are decodable by their opcode. We only have to do this during
-- assembling so we can afford it to be slow. We have to lookup the instruction
-- code in the map first, before we can use the instruction code to do a O(1)
-- lookup in the instruction table.
instance InstructionDecodableBy Opcode where
  decode opcode_ =
    let err_      = error "Invalid opcode."
        instrcode_ = fromMaybe err_ (M.lookup opcode_ instructionNames)
    in decode instrcode_

-- Instructions are decodable by their instruction code simply because they are
instance InstructionDecodableBy InstructionCode where
  decode instrcode_ = fromJust $ M.lookup instrcode_ instructions
