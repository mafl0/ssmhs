module SSM.Parser
  -- Parsing and lexing
  ( parse

  -- AST
  , Program
  , SSMasm(..)
  ) where

import           Text.Parsec hiding (parse)

import           SSM.Parser.Parser
import           SSM.Parser.Lexer
import           SSM.Parser.AST

parse :: String -> Either ParseError [SSMasm]
parse x = scanTokens x >>= parseAsm
