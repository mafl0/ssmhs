module SSM.Types where

import           Data.Word (Word32)

type SSMWord = Word32
type Address = Word32

type BinaryF = SSMWord -> SSMWord -> SSMWord
type UnaryF  = SSMWord -> SSMWord
