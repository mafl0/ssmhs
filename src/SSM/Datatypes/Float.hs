module SSM.Datatypes.Float
  ( ssmToFloat
  , ssmFromFloat
  ) where

import           Unsafe.Coerce

import           SSM.Types

ssmToFloat :: SSMWord -> Float
ssmToFloat = unsafeCoerce

ssmFromFloat :: Float -> SSMWord
ssmFromFloat = unsafeCoerce
