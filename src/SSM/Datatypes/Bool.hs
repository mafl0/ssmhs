module SSM.Datatypes.Bool
  ( ssmToBool
  , ssmFromBool
  ) where

import           SSM.Types

ssmToBool :: SSMWord -> Bool
ssmToBool 0 = False
ssmToBool _ = True

ssmFromBool :: Bool -> SSMWord
ssmFromBool False = 0x00000000
ssmFromBool True  = 0xFFFFFFFF
