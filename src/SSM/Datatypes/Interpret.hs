module SSM.Datatypes.Interpret
  ( Interpret (..)
  ) where

import           Data.Int
import           Data.Word

import           SSM.Types

import           SSM.Datatypes

-- Class of binary values (SSMWord) that can be interpreted as datatypes
class Interpret a where
  interpret :: SSMWord -> a
  asBinary  :: a -> SSMWord

instance Interpret Float where
  interpret = ssmToFloat
  asBinary  = ssmFromFloat

instance Interpret Char where
  interpret = ssmToChar
  asBinary  = ssmFromChar

instance Interpret Int8 where
  interpret = ssmToSInt8
  asBinary  = ssmFromSInt8

instance Interpret Int16 where
  interpret = ssmToSInt16
  asBinary  = ssmFromSInt16

instance Interpret Int32 where
  interpret = ssmToSInt32
  asBinary  = ssmFromSInt32

instance Interpret Word8 where
  interpret = ssmToUInt8
  asBinary  = ssmFromUInt8

instance Interpret Word16 where
  interpret = ssmToUInt16
  asBinary  = ssmFromUInt16

instance Interpret Word32 where
  interpret = ssmToUInt32
  asBinary  = ssmFromUInt32

instance Interpret Bool where
  interpret = ssmToBool
  asBinary  = ssmFromBool
