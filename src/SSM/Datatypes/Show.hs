module SSM.Datatypes.Show
  -- Hexadecimal
  ( showHex
  , parseHex

  -- Decimal
  , showDec
  , parseDec

  -- Binary
  , showBin
  , parseBin

  -- Octal
  , showOct
  , parseOct
  ) where

import           Text.Parsec
import           Text.Printf

import           SSM.Types
import           SSM.Datatypes.Interpret
import           SSM.Parser.Number

-- Convenience method for parsing numbers from a string
unsafeParseNumber :: Parsec String () SSMWord -> String -> SSMWord
unsafeParseNumber p str =
  case parse p "" str of
    Left  _   -> error "unsafeParseNumber failed to parse the number"
    Right res -> res


-- Binary
showBin :: Interpret a => a -> String
showBin x =
  let b = asBinary x
  in printf "0b%b" b

parseBin :: String -> SSMWord
parseBin = unsafeParseNumber pBin

-- Octal
showOct :: Interpret a => a -> String
showOct x =
  let b = asBinary x
  in printf "0o%o" b

parseOct :: String -> SSMWord
parseOct = unsafeParseNumber pOct

-- Decimal
showDec :: Interpret a => a -> String
showDec x =
  let b = asBinary x
  in printf "%d" b

parseDec :: String -> SSMWord
parseDec = unsafeParseNumber pDec

-- Hexadecimal
showHex :: Interpret a => a -> String
showHex x =
  let b = asBinary x
  in printf "0x%X" b

parseHex :: String -> SSMWord
parseHex = unsafeParseNumber pHex

