module SSM.Datatypes.Int
  -- Signed
  ( ssmToSInt8
  , ssmToSInt16
  , ssmToSInt32
  , ssmFromSInt8
  , ssmFromSInt16
  , ssmFromSInt32

  -- Unsigned
  , ssmFromUInt8
  , ssmFromUInt16
  , ssmFromUInt32
  , ssmToUInt8
  , ssmToUInt16
  , ssmToUInt32
  ) where

import           Data.Int
import           Data.Word

import           SSM.Types

-- Signed integers

ssmFromSInt8 :: Int8 -> SSMWord
ssmFromSInt8 = fromIntegral

ssmFromSInt16 :: Int16 -> SSMWord
ssmFromSInt16 = fromIntegral

ssmFromSInt32 :: Int32 -> SSMWord
ssmFromSInt32 = fromIntegral

ssmToSInt8 :: SSMWord -> Int8
ssmToSInt8 = fromIntegral

ssmToSInt16 :: SSMWord -> Int16
ssmToSInt16 = fromIntegral

ssmToSInt32 :: SSMWord -> Int32
ssmToSInt32 = fromIntegral

-- Unsigned integers
ssmFromUInt8 :: Word8 -> SSMWord
ssmFromUInt8 = fromIntegral

ssmFromUInt16 :: Word16 -> SSMWord
ssmFromUInt16 = fromIntegral

ssmFromUInt32 :: Word32 -> SSMWord
ssmFromUInt32 = fromIntegral

ssmToUInt8 :: SSMWord -> Word8
ssmToUInt8 = fromIntegral

ssmToUInt16 :: SSMWord -> Word16
ssmToUInt16 = fromIntegral

ssmToUInt32 :: SSMWord -> Word32
ssmToUInt32 = fromIntegral

