module SSM.Datatypes.Char
  ( ssmToChar
  , ssmFromChar
  ) where

import           Data.Char

import           SSM.Types

ssmToChar :: SSMWord -> Char
ssmToChar = chr . fromIntegral

ssmFromChar :: Char -> SSMWord
ssmFromChar = fromIntegral . ord
