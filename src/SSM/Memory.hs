{- The SSM is defined by the state of its memory. This module includes the basic
 - functionality to manipulate the memory and thus forms the basis of program
 - execution.
 -
 - Memory map layout
 -
 - Variables are P, which is the final address of the program. And RAM, which is
 - the amount of RAM currently on the machine. Obviously you cannot write to
 - more addresses than there is RAM available.
 -
 - P depends on the size of the program, and defines S, the beginning of the
 - stack. If the program is 10 words long, then S is 11 addresses higher than
 - the end of the memory mapped region.
 -
 - The memory mapped region is used to map registers. In the future i plan on
 - adding a frame buffer and an audio output buffer.
 -
 -         +-----------------+ Begin memory mapped region
 -   0x000 | Program Counter | Register file
 -         |        .        |
 -         |        .        |
 -         |        .        |
 -   0x007 | Status register |
 -         +-----------------+
 -   0x008 | Zero register   | Special register file
 -   0x009 | Random register |
 -         +-----------------+ End memory mapped region
 -   0x00A | Program start   |
 -         |        .        |
 -         |        .        |
 -         |        .        |
 -       P | Program end     |
 -         +-----------------+ program/stack memory barrier
 -       S | Stack start     | S = P + 1
 -         |        .        |
 -         |        .        |
 -         |        .        |
 -   0x7FF | Stack end       |
 -         +-----------------+ stack/heap memory barrier
 -   0x800 | Heap start      |
 -         |        .        |
 -         |        .        |
 -         |        .        |
 - RAM - 1 | Heap end        |
 -         +-----------------+ final memory barrier, no more addresses past here
 -}

module SSM.Memory
  ( Memory()
  , SSM
  , SSMVoid
  -- Stack
  , pop
  , push

  -- Heap

  -- Register access (load/store)
  , RegisterAddressableBy(..)
  , loadRegister
  , storeRegister

  -- Memory primitives
  , readMemory
  , writeMemory

  -- Constants
  , amountOfMemory
  , heapSize
  , heapStart
  , memoryMapSize

  -- Initialisers
  , newMemory
  , initMemory
  ) where

import           Data.Array.IO
import qualified Data.Array as A
import           System.Random
import           Text.Printf

import           SSM.Memory.Stack
import           SSM.Memory.Core
import           SSM.Memory.Register
import           SSM.Types

-- Memory mapping size in the beginning of the address space, This value points
-- to the _first_ instruction to be executed so the last address in the
-- map-region is memoryMapSize - 1
memoryMapSize :: Address
memoryMapSize = 0x000A

-- Amount of ram that is available to the machine
amountOfMemory :: Address
amountOfMemory = 0x1000

-- Size of the heap. The heap pointer will start this many words before the end
-- of memory which is amountOfMemory - 1 - heapSize.
--
-- Important: heapSize < amountOfMemory, advised is much less
heapSize :: Address
heapSize = 0x800

heapStart :: Address
heapStart = amountOfMemory - heapSize - 1

-- New empty memory map
newMemory :: IO Memory
newMemory = newListArray (0, amountOfMemory - 1) (repeat 0)

-- Initialise a new memory map from an addressed program
initMemory :: [(Address, SSMWord)] -> IO Memory
initMemory p =
  let sp = fst (last p)
  in do
    randomRegisterVal <- liftIO $ (randomIO :: IO SSMWord)
    mem  <- newMemory

    -- Set register values
    writeArray mem 0x000 memoryMapSize     -- PC
    writeArray mem 0x001 sp                -- SP
    writeArray mem 0x002 sp                -- MP
    writeArray mem 0x003 heapStart         -- HP
    writeArray mem 0x008 0                 -- devzero
    writeArray mem 0x009 randomRegisterVal -- devrandom

    forM_ p (\(addr, val) -> writeArray mem addr val)

    return mem

