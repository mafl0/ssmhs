module Test.Programs where

import           Test.Definition

testProgramPrefix :: FilePath
testProgramPrefix = "/home/martijn/projects/haskell/ssmhs/testprograms/"

mkTest fp pre post =
  Test
    (testProgramPrefix ++ fp)
    pre
    post

tests :: [Test]
tests =
  [ mkTest "ldc.ssm"
      [ (0x0, 0xa) -- PC is 0x0
      , (0x1, 0xd) -- SP is 0xd
      , (0xd, 0x0) -- First stack value is 0
      ]
      [ (0x0, 0xa + 2) -- PC has advanced
      , (0x1, 0xd + 1) -- SP has advanced
      , (0xd, -1)      -- Value on stack
      ]
  ]

