{-# LANGUAGE RecordWildCards #-}

module Test.Definition
  -- Constructors and types
  ( PreCondition()
  , PostCondition()
  , Test(..)

  -- Test runner
  , runTest
  ) where

import           Control.Monad.Reader
import           Data.Array (Array)
import           Data.Array.IO
import           Data.List
import qualified Data.Array as A

import           SSM.Compiler
import           SSM.Instructions
import           SSM.Memory
import           SSM.Parser
import           SSM.Types

{- Pre- and postconditions are simply lists of addresses and their values.
 - When we start a test, the Precondition will be set in the memory. The
 - Postcondition will be checked after the computation has completed.
 -}
type PreCondition  = [(Address, SSMWord)]
type PostCondition = [(Address, SSMWord)]

data Test = Test
  { filePath      :: FilePath
  , precondition  :: PreCondition
  , postcondition :: PostCondition
  }

-- Runs a test. It is not checked if a file exists. The prefix to the test
-- directory is hard-coded. Post- and preconditions are simple files
runTest :: Test -> IO ()
runTest Test{..} =
  do
    content <- readFile filePath
    case parse content of
      Left error_ -> fail (show error_)
      Right asm   ->
        do
          mem <- assembleSSM asm

          -- Prepare the postcondition
          forM_ postcondition (\(addr, val) -> writeArray mem addr val)

          runReaderT executeSSM mem

          mem_post <- A.assocs <$> freeze mem

          if (mem_post `intersect` postcondition) == postcondition then
            putStrLn $ "Test passed: " ++ filePath
          else
            putStrLn $ "Test failed: " ++ filePath


