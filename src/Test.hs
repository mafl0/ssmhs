module Test where

import Test.Definition
import Test.Programs

runTests :: IO ()
runTests = mapM_ runTest tests
