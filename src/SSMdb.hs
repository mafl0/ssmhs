module Main where

{- The main SSM debugger binary
 -
 - Run like: ssmdb [options] <filename>
 -
 - options:
 -   --verbose  -v     dump memory during execution (unsupported)
 -   --memory   -m     memory size, default is 4K   (unsupported)
 -   --heapsize -h     heap size, default is 2K     (unsupported)
 -
 - Will run an interactive debugger
 -}

main :: IO ()
main = undefined
