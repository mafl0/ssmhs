#include "ssmhs.h"

module Main where

import           Control.Monad.Reader
import           System.Directory
import           System.IO
import           Data.List

import           SSM.Compiler
import           SSM.Instructions

oneFile :: FilePath -> IO ()
oneFile fp =
  do
    putStrLn ("===\nRunning: " ++ fp ++ "\n===")
    mem <- compileIO fp
    runReaderT (executeSSMDebug) mem

--main = oneFile "testprograms/fib-recursive.ssm"


main :: IO ()
main =
  let prefix = "testprograms/instructions/"
  in do
    files <- fmap (\x -> prefix ++ x) . sort <$> listDirectory prefix
    mapM_ oneFile files

